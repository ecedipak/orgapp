# Generated by Django 2.2.7 on 2019-12-04 10:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orgapp', '0003_subscriber'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='image',
            field=models.FileField(upload_to='gallery'),
        ),
    ]
