from django.contrib import admin
from .models import *


admin.site.register([OrgnizationalInformation, Slider, MemberType,
                     Member, Publication, Download, Album, Image,
                     Article, Message,Event, Subscriber])
