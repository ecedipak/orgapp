from django.contrib.auth.models import User
from django.shortcuts import render
from django.views.generic import *
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.core.mail import send_mail, send_mass_mail
from django.conf import settings
from .models import *
from .forms import *
from django.contrib.auth import authenticate, login, logout



# admin views


class AdminRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            pass
        else:
            return redirect("/signin/")

        return super().dispatch(request, *args, **kwargs)




class SigninView(FormView):
    template_name = "admintemplates/adminlogin.html"
    form_class = SigninForm
    success_url = reverse_lazy('orgapp:adminorganization')

    def form_valid(self, form):
        uname = form.cleaned_data["username"]
        pword = form.cleaned_data["password"]
        user = authenticate(username=uname, password=pword)
        if user is not None:
            login(self.request, user)
        else:
            return render(self.request, "admintemplates/adminlogin.html",
                          {"error": "Invalid Username or Password",
                           "form": form
                           })

        return super().form_valid(form)





class SignoutView(View):
    def get(self, request):
        logout(request)
        return redirect("/signin/")


class AdminPasswordChangeView(AdminRequiredMixin,FormView):
    template_name = 'admintemplates/passwordchange.html'
    form_class = PasswordChange
    success_url = reverse_lazy('orgapp:signin')

    def form_valid(self, form):
        current_user = self.request.user
        current_user.set_password(form.cleaned_data['password'])
        current_user.save()
        
        return super().form_valid(form)








class AdminOrganizationView(AdminRequiredMixin, ListView):
    template_name = 'admintemplates/adminorganization.html'
    queryset = OrgnizationalInformation.objects.all()
    context_object_name = 'organizationlist'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['org'] = OrgnizationalInformation.objects.first()
        context['album'] = Album.objects.count()
        context['member'] = Member.objects.count()
        context['article'] = Article.objects.count()
        context['event'] = Event.objects.count()


        return context


class AdminOrganizationUpdateView(AdminRequiredMixin, UpdateView):
    template_name = 'admintemplates/adminorganizationupdate.html'
    form_class = OrganizationForm
    model = OrgnizationalInformation
    success_url = reverse_lazy('orgapp:adminorganization')


class AdminMemberListView(AdminRequiredMixin, ListView):
    template_name = 'admintemplates/adminmemberlist.html'
    queryset = Member.objects.all().order_by("-id")
    context_object_name = 'memberlist'


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['album'] = Album.objects.count()
        context['member'] = Member.objects.count()
        context['article'] = Article.objects.count()
        context['event'] = Event.objects.count()


        return context


class AdminMemberCreateView(AdminRequiredMixin, CreateView):
    template_name = 'admintemplates/adminmembercreate.html'
    form_class = MemberForm
    success_url = reverse_lazy("orgapp:adminmemberlist")


class AdminMemberUpdateView(AdminRequiredMixin, UpdateView):
    template_name = 'admintemplates/adminmemberupdate.html'
    model = Member
    form_class = MemberForm
    success_url = reverse_lazy("orgapp:adminmemberlist")


class AdminMemberDeleteView(AdminRequiredMixin, DeleteView):
    template_name = 'admintemplates/adminmemberdelete.html'
    model = Member
    success_url = reverse_lazy("orgapp:adminmemberlist")


class AdminSliderListView(AdminRequiredMixin, ListView):
    template_name = "admintemplates/adminsliderlist.html"
    queryset = Slider.objects.all().order_by("-id")
    context_object_name = "sliderlist"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['album'] = Album.objects.count()
        context['member'] = Member.objects.count()
        context['article'] = Article.objects.count()
        context['event'] = Event.objects.count()


        return context


class AdminSliderCreateView(AdminRequiredMixin, CreateView):
    template_name = "admintemplates/adminslidercreate.html"
    form_class = SliderForm
    success_url = reverse_lazy('orgapp:adminsliderlist')


class AdminSliderUpdateView(AdminRequiredMixin, UpdateView):
    template_name = "admintemplates/adminsliderupdate.html"
    form_class = SliderForm
    model = Slider
    success_url = reverse_lazy("orgapp:adminsliderlist")


class AdminSliderDeleteView(AdminRequiredMixin, DeleteView):
    template_name = "admintemplates/adminsliderdelete.html"
    model = Slider
    success_url = reverse_lazy("orgapp:adminsliderlist")


class AdminPublicationListView(AdminRequiredMixin, ListView):
    template_name = "admintemplates/adminpublicationlist.html"
    queryset = Publication.objects.all().order_by("-id")
    context_object_name = "publicationlist"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['album'] = Album.objects.count()
        context['member'] = Member.objects.count()
        context['article'] = Article.objects.count()
        context['event'] = Event.objects.count()


        return context


class AdminPublicationCreateView(AdminRequiredMixin, CreateView):
    template_name = 'admintemplates/adminpublicationcreate.html'
    form_class = PublicationForm
    success_url = reverse_lazy("orgapp:adminpublicationlist")


class AdminPublicationUpdateView(AdminRequiredMixin, UpdateView):
    template_name = "admintemplates/adminpublicationupdate.html"
    form_class = PublicationForm
    model = Publication
    success_url = reverse_lazy("orgapp:adminpublicationlist")


class AdminPublicationDeleteView(AdminRequiredMixin, DeleteView):
    template_name = "admintemplates/adminpublicationdelete.html"
    model = Publication
    success_url = reverse_lazy("orgapp:adminpublicationlist")


class AdminDownloadListView(AdminRequiredMixin, ListView):
    template_name = "admintemplates/admindownloadlist.html"
    queryset = Download.objects.all().order_by("-id")
    context_object_name = "downloadlist"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['album'] = Album.objects.count()
        context['member'] = Member.objects.count()
        context['article'] = Article.objects.count()
        context['event'] = Event.objects.count()


        return context


class AdminDownloadCreateView(AdminRequiredMixin, CreateView):
    template_name = "admintemplates/admindownloadcreate.html"
    form_class = DownloadForm
    success_url = reverse_lazy("orgapp:admindownloadlist")


class AdminDownloadUpdateView(AdminRequiredMixin, UpdateView):
    template_name = "admintemplates/admindownloadupdate.html"
    form_class = DownloadForm
    model = Download
    success_url = reverse_lazy("orgapp:admindownloadlist")


class AdminDownloadDeleteView(AdminRequiredMixin, DeleteView):
    template_name = 'admintemplates/admindownloaddelete.html'
    model = Download
    success_url = reverse_lazy("orgapp:admindownloadlist")


class AdminAlbumListView(AdminRequiredMixin, ListView):
    template_name = "admintemplates/adminalbumlist.html"
    queryset = Album.objects.all().order_by("-id")
    context_object_name = "albumlist"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['album'] = Album.objects.count()
        context['member'] = Member.objects.count()
        context['article'] = Article.objects.count()
        context['event'] = Event.objects.count()


        return context


class AdminAlbumCreateView(AdminRequiredMixin, CreateView):
    template_name = "admintemplates/adminalbumcreate.html"
    form_class = AlbumForm
    success_url = reverse_lazy("orgapp:adminalbumlist")

    def form_valid(self, form):
        album = form.save()
        images = self.request.FILES.getlist("images")
        for image in images:
            Image.objects.create(album=album, image=image)

        return super().form_valid(form)


class AdminAlbumUpdateView(AdminRequiredMixin, UpdateView):
    template_name = "admintemplates/adminalbumupdate.html"
    form_class = AlbumForm
    model = Album
    success_url = reverse_lazy("orgapp:adminalbumlist")

    def form_valid(self, form):
        album = form.save()
        images = self.request.FILES.getlist("images")
        for image in images:
            Image.objects.create(album=album, image=image)

        return super().form_valid(form)


class AdminAlbumDeleteView(AdminRequiredMixin, DeleteView):
    template_name = "admintemplates/adminalbumdelete.html"
    model = Album
    success_url = reverse_lazy("orgapp:adminalbumlist")


class AdminImageListView(AdminRequiredMixin, ListView):
    template_name = "admintemplates/adminimagelist.html"
    queryset = Image.objects.all().order_by("-id")
    context_object_name = "imagelist"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['album'] = Album.objects.count()
        context['member'] = Member.objects.count()
        context['article'] = Article.objects.count()
        context['event'] = Event.objects.count()


        return context


class AdminImageCreateView(AdminRequiredMixin, CreateView):
    template_name = "admintemplates/adminimagecreate.html"
    form_class = ImageForm
    success_url = reverse_lazy("orgapp:adminimagelist")


class AdminImageUpdateView(AdminRequiredMixin, UpdateView):
    template_name = "admintemplates/adminimageupdate.html"
    form_class = ImageForm
    model = Image
    success_url = reverse_lazy("orgapp:adminimagelist")


class AdminImageDeleteView(AdminRequiredMixin, DeleteView):
    template_name = "admintemplates/adminimagedelete.html"
    model = Image
    success_url = reverse_lazy("orgapp:adminimagelist")

class AdminContactListView(AdminRequiredMixin, ListView):
    template_name = "admintemplates/admincontactlist.html"
    queryset = Message.objects.all().order_by("-id")
    context_object_name = "messagelist"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['album'] = Album.objects.count()
        context['member'] = Member.objects.count()
        context['article'] = Article.objects.count()
        context['event'] = Event.objects.count()


        return context


class AdminArticleListView(AdminRequiredMixin, ListView):
    template_name = "admintemplates/adminarticlelist.html"
    queryset = Article.objects.all()
    context_object_name = "articlelist"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['album'] = Album.objects.count()
        context['member'] = Member.objects.count()
        context['article'] = Article.objects.count()
        context['event'] = Event.objects.count()


        return context


class AdminArticleCreateView(AdminRequiredMixin, CreateView):
    template_name = "admintemplates/adminarticlecreate.html"
    form_class = ArticleForm
    success_url = reverse_lazy("orgapp:adminarticlelist")


class AdminArticleUpdateView(AdminRequiredMixin, UpdateView):
    template_name = "admintemplates/adminarticleupdate.html"
    form_class = ArticleForm
    model = Article
    success_url = reverse_lazy("orgapp:adminarticlelist")


class AdminArticleDeleteView(AdminRequiredMixin, DeleteView):
    template_name = "admintemplates/adminarticledelete.html"
    model = Article
    success_url = reverse_lazy("orgapp:adminarticlelist")


class AdminEventListView(AdminRequiredMixin, ListView):
    template_name = "admintemplates/admineventlist.html"
    queryset = Event.objects.all().order_by("-id")
    context_object_name = "eventlist"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['album'] = Album.objects.count()
        context['member'] = Member.objects.count()
        context['article'] = Article.objects.count()
        context['event'] = Event.objects.count()


        return context


class AdminEventCreateView(AdminRequiredMixin, CreateView):
    template_name = "admintemplates/admineventcreate.html"
    form_class = EventForm
    success_url = reverse_lazy("orgapp:admineventlist")


class AdminEventUpdateView(AdminRequiredMixin, UpdateView):
    template_name = "admintemplates/admineventupdate.html"
    form_class = EventForm
    model = Event
    success_url = reverse_lazy("orgapp:admineventlist")


class AdminEventDeleteView(AdminRequiredMixin, DeleteView):
    template_name = "admintemplates/admineventdelete.html"
    model = Event
    success_url = reverse_lazy("orgapp:admineventlist")















# client views

class BaseMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["org"] = OrgnizationalInformation.objects.first()
        context["subs"] = SubscriberForm
        context["event"] = Event.objects.all()

        return context


class HomeView(BaseMixin, TemplateView):
    template_name = 'clienttemplates/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["sliders"] = Slider.objects.all()
        context["members"] = Member.objects.all()
        context["albums"] = Album.objects.all()
        context["articles"] = Article.objects.all().order_by("-id")
        context["events"] = Event.objects.all().order_by("-id")

        return context


class AboutView(BaseMixin, TemplateView):
    template_name = 'clienttemplates/about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["orgn"] = OrgnizationalInformation.objects.first()
        context["articles"] = Article.objects.all()

        return context


class ContactCreateView(BaseMixin, CreateView):
    template_name = "clienttemplates/contact.html"
    form_class = ContactForm
    success_url = reverse_lazy("orgapp:contact")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["orgna"] = OrgnizationalInformation.objects.first()

        return context


class EventListView(BaseMixin, ListView):
    template_name = 'clienttemplates/eventlist.html'
    queryset = Event.objects.all().order_by("-id")
    context_object_name = "eventlist"


class EventDetailView(BaseMixin, DetailView):
    template_name = 'clienttemplates/eventdetail.html'
    model = Event
    context_object_name = "events"


class AlbumView(BaseMixin, ListView):
    template_name = 'clienttemplates/album.html'
    queryset = Album.objects.all().order_by("-id")
    context_object_name = "albumlist"


class AlbumDetailView(BaseMixin, DetailView):
    template_name = 'clienttemplates/albumdetail.html'
    model = Album
    context_object_name = 'album'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["img"] = Image.objects.all()

        return context


class MemberView(BaseMixin, TemplateView):
    template_name = 'clienttemplates/member.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["members"] = Member.objects.all()

        return context


class ArticleView(BaseMixin, ListView):
    template_name = 'clienttemplates/article.html'
    queryset = Article.objects.all().order_by("-id")
    context_object_name = "articlelist"


class ArticleDetailView(BaseMixin, DetailView):
    template_name = 'clienttemplates/articledetail.html'
    model = Article
    context_object_name = "article"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = ArticleCommentForm

        return context


class SubscriberCreateView(BaseMixin, CreateView):
    template_name = 'clienttemplates/clientbase.html'
    form_class = SubscriberForm
    success_url = reverse_lazy("orgapp:home")

    def form_valid(self, form):
        form_email = form.cleaned_data["email"]
        if Subscriber.objects.filter(email=form_email).exists():
            return render(self.request, "clienttemplates/clientbase.html", {"error": "Subscriber already exists"})

        subject = 'Site contact form'
        from_email = settings.EMAIL_HOST_USER
        contact_message = "Thanks for subscribing us."
        send_mail(subject,
                  contact_message,
                  from_email,
                  [form_email],
                  fail_silently=False)

        return super().form_valid(form)


class PublicationView(BaseMixin, ListView):
    template_name = "clienttemplates/publication.html"
    queryset = Publication.objects.all()
    context_object_name = "publicationlist"


class PublicationDetailView(BaseMixin, DetailView):
    template_name = "clienttemplates/publicationdetail.html"
    model = Publication
    context_object_name = "publication"


class DownloadView(BaseMixin, ListView):
    template_name = "clienttemplates/download.html"
    queryset = Download.objects.all().order_by("-id")
    context_object_name = "downloadlist"
