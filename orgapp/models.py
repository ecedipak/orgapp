from django.db import models
from django.contrib.auth.models import User



class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(null=True, blank=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class OrgnizationalInformation(TimeStamp):
    name = models.CharField(max_length=200)
    logo = models.ImageField(upload_to="organization")
    profile_image = models.ImageField(upload_to="organization")
    vat_pan = models.CharField(max_length=200, null=True, blank=True)
    address = models.CharField(max_length=500)
    slogan = models.CharField(max_length=500, null=True, blank=True)
    contact_no = models.CharField(max_length=200)
    alt_contact_no = models.CharField(max_length=200, null=True, blank=True)
    map_location = models.CharField(max_length=200)
    email = models.EmailField()
    alt_email = models.EmailField(null=True, blank=True)
    about_us = models.TextField()
    constitution = models.TextField()

    facebook = models.CharField(max_length=200)
    instagram = models.CharField(max_length=200, null=True, blank=True)
    twitter = models.CharField(max_length=200, null=True, blank=True)
    youtube = models.CharField(max_length=200, null=True, blank=True)
    whatsapp = models.CharField(max_length=200, null=True, blank=True)
    viber = models.CharField(max_length=200, null=True, blank=True)
    terms_and_conditions = models.TextField(null=True, blank=True)
    privacy_policy = models.TextField(null=True, blank=True)
    # cookies_policy = models.TextField(null=True, blank=True)
    show_popup = models.BooleanField(default=False)
    popup_image = models.ImageField(
        upload_to="organization", null=True, blank=True)

    messenger_script = models.TextField(null=True, blank=True)
    google_analytics_script = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class MemberType(TimeStamp):
    title = models.CharField(max_length=200)

    def __str__(self):
        return self.title


class Member(TimeStamp):
    name = models.CharField(max_length=200)
    member_type = models.ForeignKey(MemberType, on_delete=models.CASCADE)
    post = models.CharField(max_length=200)
    image = models.ImageField(upload_to="members")
    mobile = models.CharField(max_length=40, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    about = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name + "(" + self.post + ")"


class Slider(TimeStamp):
    title = models.CharField(max_length=200)
    caption = models.CharField(max_length=500, null=True, blank=True)
    image = models.ImageField(upload_to="sliders")

    def __str__(self):
        return self.title


class Publication(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to="publications")
    description = models.TextField()

    def __str__(self):
        return self.title


class Download(TimeStamp):
    title = models.CharField(max_length=200)
    short_details = models.CharField(max_length=500, null=True, blank=True)
    file = models.FileField(upload_to="sliders")

    def __str__(self):
        return self.title


class Album(TimeStamp):
    title = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title + str(self.image_set.count())


class Image(TimeStamp):
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="gallery")

    def __str__(self):
        return self.album.title


class Article(TimeStamp):
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True, null=True, blank=True)
    image = models.ImageField(upload_to="articles")
    content = models.TextField()

    def __str__(self):
        return self.title


class Event(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to="events")
    date = models.DateTimeField()
    venue = models.CharField(max_length=300)
    description = models.TextField()

    def __str__(self):
        return self.title


class Message(TimeStamp):
    name = models.CharField(max_length=200)
    mobile = models.CharField(max_length=50)
    email = models.EmailField(null=True, blank=True)
    subject = models.CharField(max_length=200, null=True, blank=True)
    message = models.TextField()

    def __str__(self):
        return self.name


class Subscriber(TimeStamp):
    email = models.EmailField()

    def __str__(self):
        return self.email
