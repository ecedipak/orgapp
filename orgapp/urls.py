from django.urls import path
from .views import *


app_name = "orgapp"

urlpatterns = [
    # Admin Urls
    path('signin/', SigninView.as_view(), name="signin"),
    path('signout/', SignoutView.as_view(), name="signout"),
    path('admin/organization/', AdminOrganizationView.as_view(),
         name="adminorganization"),
    path('admin/organization/<int:pk>/edit/',
         AdminOrganizationUpdateView.as_view(), name="adminorganizationupdate"),
    path('admin/member/list/', AdminMemberListView.as_view(), name="adminmemberlist"),
    path('admin/member/create/', AdminMemberCreateView.as_view(),
         name="adminmembercreate"),
    path('admin/member/<int:pk>/edit/',
         AdminMemberUpdateView.as_view(), name="adminmemberupdate"),
    path('admin/member/<int:pk>/delete/',
         AdminMemberDeleteView.as_view(), name="adminmemberdelete"),
    path('admin/slider/list/', AdminSliderListView.as_view(), name='adminsliderlist'),
    path('admin/slider/create/', AdminSliderCreateView.as_view(),
         name='adminslidercreate'),
    path('admin/slider/<int:pk>/edit/',
         AdminSliderUpdateView.as_view(), name='adminsliderupdate'),
    path('admin/slider/<int:pk>/delete/',
         AdminSliderDeleteView.as_view(), name='adminsliderdelete'),
    path('admin/publication/list/', AdminPublicationListView.as_view(),
         name='adminpublicationlist'),
    path('admin/publication/create/', AdminPublicationCreateView.as_view(),
         name='adminpublicationcreate'),
    path('admin/publication/<int:pk>/edit/',
         AdminPublicationUpdateView.as_view(), name='adminpublicationupdate'),
    path('admin/publication/<int:pk>/delete/',
         AdminPublicationDeleteView.as_view(), name='adminpublicationdelete'),
    path('admin/contact/list/', AdminContactListView.as_view(),
         name='admincontactlist'),
    path('admin/download/list/', AdminDownloadListView.as_view(),
         name='admindownloadlist'),
    path('admin/download/create/', AdminDownloadCreateView.as_view(),
         name='admindownloadcreate'),
    path('admin/download/<int:pk>/edit/',
         AdminDownloadUpdateView.as_view(), name='admindownloadupdate'),
    path('admin/download/<int:pk>/delete/',
         AdminDownloadDeleteView.as_view(), name='admindownloaddelete'),
    path('admin/album/list/', AdminAlbumListView.as_view(), name='adminalbumlist'),
    path('admin/album/create/', AdminAlbumCreateView.as_view(),
         name='adminalbumcreate'),
    path('admin/album/<int:pk>/edit/',
         AdminAlbumUpdateView.as_view(), name='adminalbumupdate'),
    path('admin/album/<int:pk>/delete/',
         AdminAlbumDeleteView.as_view(), name='adminalbumdelete'),
    path('admin/image/list/', AdminImageListView.as_view(), name='adminimagelist'),
    path('admin/image/create/', AdminImageCreateView.as_view(),
         name='adminimagecreate'),
    path('admin/image/<int:pk>/edit/',
         AdminImageUpdateView.as_view(), name='adminimageupdate'),
    path('admin/image/<int:pk>/delete/',
         AdminImageDeleteView.as_view(), name='adminimagedelete'),
    path('admin/article/list/', AdminArticleListView.as_view(),
         name='adminarticlelist'),
    path('admin/article/create/', AdminArticleCreateView.as_view(),
         name='adminarticlecreate'),
    path('admin/article/<int:pk>/edit/',
         AdminArticleUpdateView.as_view(), name='adminarticleupdate'),
    path('admin/article/<int:pk>/delete/',
         AdminArticleDeleteView.as_view(), name='adminarticledelete'),
    path('admin/event/list/', AdminEventListView.as_view(), name='admineventlist'),
    path('admin/event/create/', AdminEventCreateView.as_view(),
         name='admineventcreate'),
    path('admin/event/<int:pk>/edit/',
         AdminEventUpdateView.as_view(), name='admineventupdate'),
    path('admin/event/<int:pk>/delete/',
         AdminEventDeleteView.as_view(), name='admineventdelete'),
    path('admin/password/change', AdminPasswordChangeView.as_view(), name= "passwordchanged"),










    # client urls
    path('', HomeView.as_view(), name="home"),
    path('about/', AboutView.as_view(), name="about"),
    path('contact/', ContactCreateView.as_view(), name="contact"),
    path('event/list/', EventListView.as_view(), name="eventlist"),
    path('event/<int:pk>/detail/', EventDetailView.as_view(), name="eventdetail"),
    path('album/', AlbumView.as_view(), name="albumlist"),
    path('album/detail/<int:pk>/', AlbumDetailView.as_view(), name="albumdetail"),
    path('member/', MemberView.as_view(), name="memberlist"),
    path('article/', ArticleView.as_view(), name="articlelist"),
    path('article/<int:pk>/detail/',
         ArticleDetailView.as_view(), name="articledetail"),
    path("subscriber/", SubscriberCreateView.as_view(), name="subscriber"),
    path("publication/", PublicationView.as_view(), name="publication"),
    path("publication/<int:pk>/detail/", PublicationDetailView.as_view(), name="publicationdetail"),
    path("download/", DownloadView.as_view(), name="download"),
















]
