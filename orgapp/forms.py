
from django.contrib.auth.models import User
from .models import *
from django_summernote.widgets import SummernoteWidget
from django import forms


class SigninForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': "enter username...."
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'enter your password...'
    }))


class PasswordChange(forms.Form):

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'enter your password...'
    }))

    changepassword = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'enter changed password...'
    }))


    def clean_confirm_password(self):
        password =self.cleaned_data.get('password')
        changepassword = self.cleaned_data.get('changepassword')

        if password != changepassword:
            
            raise forms.ValidationError("password and changepassword didn't match")

        return changepassword







class OrganizationForm(forms.ModelForm):
    class Meta:
        model = OrgnizationalInformation
        fields = ["name", "logo", "email", "alt_email", "vat_pan", "address",
                  "slogan", "about_us", "constitution", "profile_image", "map_location", "alt_contact_no",
                  "facebook", "youtube", "instagram", "terms_and_conditions", "privacy_policy", "contact_no"]
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'logo': forms.ClearableFileInput(attrs={
                'class': 'form-control',
            }),
            'email': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'alt_email': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'vat_pan': forms.TextInput(attrs={
                'class': 'form-control',
            }),

            'address': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'slogan': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'about_us': SummernoteWidget(),

            'constitution': forms.Textarea(attrs={
                'class': 'form-control',
            }),
            'profile_image': forms.ClearableFileInput(attrs={
                'class': 'form-control',
            }),
            'map_location': forms.TextInput(attrs={
                'class': 'form-control',
            }),

            'contact_no': forms.TextInput(attrs={
                'class': 'form-control',
            }),

            'alt_contact_no': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'facebook': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'youtube': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'instagram': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'terms_and_conditions': SummernoteWidget(),

            'privacy_policy': forms.Textarea(attrs={
                'class': 'form-control',
            }),

        }


class MemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = ['name', 'member_type', 'post',
                  'image', 'mobile', 'email', 'about']

        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'member_type': forms.Select(attrs={
                'class': 'form-control',
            }),
            'post': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'image': forms.ClearableFileInput(attrs={
                'class': 'form-control',
            }),

            'mobile': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'email': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'about': SummernoteWidget(),

        }


class SliderForm(forms.ModelForm):
    class Meta:
        model = Slider
        fields = ['title', 'image', 'caption']

        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter title...'
            }),
            'image': forms.ClearableFileInput(attrs={
                'class': 'form-control',
            }),
            'caption': forms.Textarea(attrs={
                'class': 'form-control',
            }),
        }


class PublicationForm(forms.ModelForm):
    class Meta:
        model = Publication
        fields = ['title', 'image', 'description']

        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter title...'
            }),
            'image': forms.ClearableFileInput(attrs={
                'class': 'form-control',
            }),
            'description': SummernoteWidget(),
        }


class DownloadForm(forms.ModelForm):
    class Meta:
        model = Download
        fields = ['title', 'short_details', 'file']

        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter title...'
            }),
            'short_details': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'file': forms.FileInput(attrs={
                'class': 'form-control',
            }),
        }


class AlbumForm(forms.ModelForm):
    images = forms.FileField(widget=forms.FileInput(attrs={
        'multiple': True
    }))

    class Meta:
        model = Album
        fields = ['title', 'description']

        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter title...'
            }),
            'description': SummernoteWidget(),

        }


class ImageForm(forms.ModelForm):
    #   image = forms.FileField(widget=forms.FileInput(attrs={
    #     'multiple': True
    # }))

    class Meta:
        model = Image
        fields = ['album', 'image']

        widgets = {
            'album': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'Enter title...'
            }),
            'image': forms.ClearableFileInput(attrs={
                'class': 'form-control',
                'multiple': True
            }),

        }


class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'image', 'slug', 'content']

        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter title...'
            }),
            'image': forms.ClearableFileInput(attrs={
                'class': 'form-control',
            }),

            'slug': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter slug...'
            }),
            'content': forms.Textarea(attrs={
                'class': 'form-control',
            }),


        }


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['title', 'image', 'date', 'venue', 'description']

        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter title...'
            }),
            'image': forms.ClearableFileInput(attrs={
                'class': 'form-control',
            }),

            'date': forms.DateTimeInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter date...'
            }),
            'venue': forms.TextInput(attrs={
                'class': 'form-control',
            }),

            'description': SummernoteWidget(),


        }


class ContactForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['name', 'mobile', 'email', 'message']

        widgets = {

            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter your name...'
            }),
            'mobile': forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': "enter your number..."
            }),

            'email': forms.EmailInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter your email...'
            }),
            'message': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': "enter your message..."
            }),

        }


class ArticleCommentForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['name', 'mobile', 'email', 'message']

        widgets = {

            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter your name...'
            }),
            'mobile': forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': "enter your number..."
            }),

            'email': forms.EmailInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter your email...'
            }),
            'message': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': "enter your message..."
            }),

        }


class SubscriberForm(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = "__all__"
        widgets = {
            'email': forms.EmailInput(attrs={
                'class': 'mc-form form',
                'placeholder': 'Enter email...'
            }),
        }
